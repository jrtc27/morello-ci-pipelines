#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo userdata.tar.xz "${USERDATA_URL}"
tar xf userdata.tar.xz

test="bionic-unit-tests-static"
for test_path in ${TEST_PATHS}; do
  adb push "data/${test_path}/${test}/${test}" \
    "/data/${test_path}/${test}/${test}"
  adb shell "/data/${test_path}/${test}/${test} --gtest_filter=${GTEST_FILTER}" 2>&1 \
    | tee /tmp/results.log

  grep "\[==========\]" /tmp/results.log
  grep "\[  PASSED  \]" /tmp/results.log
  grep "\[  SKIPPED \]" /tmp/results.log
  grep "\[  FAILED  \]" /tmp/results.log

  grep -q "\[  FAILED  \]" /tmp/results.log
  status=$?
  echo "<LAVA_SIGNAL_STARTTC ${test_path}-${test}>"
  if [ "$status" -eq 1 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test_path}-${test}>"
  rm -f /tmp/results.log
done
