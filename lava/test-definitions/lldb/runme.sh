#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo tests.tar.xz "${LLDB_TESTS_URL}"
tar xf tests.tar.xz

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo toolchain.tar.xz "${TC_URL}"
tar xf toolchain.tar.xz

echo "Done downloading lldb tests and toolchain..."
ANDROID_OUT="${PWD}/lldb_tests/"
LLDB_SERVER_PATH="/data/local/tmp/lldb-server"
TOOLCHAIN_DIR="${PWD}/clang-current/"

adb push ${TOOLCHAIN_DIR}/runtimes_ndk_cxx/aarch64/lldb-server ${LLDB_SERVER_PATH}

# Remove the binaries from the device
adb shell rm -rf /data/nativetestc64

pip install psutil

apt update; apt install -y libncurses5
echo "Done pushing lldb-server..."
cd ${ANDROID_OUT}/lldb/
# FIXME: ORIGINAL_SOURCE_PREFIX is fixed to be the path from build source.
# This can cause problem if build script is updated
lit -v -j 1 . --param ANDROID_OUT=${ANDROID_OUT}/ --param LLDB_SERVER_PORT=5052 --param LLVM_TOOLS_DIR=${TOOLCHAIN_DIR}/bin --param ORIGINAL_SOURCE_PREFIX=test/lldb --param LLDB_SERVER=${LLDB_SERVER_PATH} | tee logs.txt

passing_tests=$(cat logs.txt | grep "^PASS:" | cut -d"/"  -f2 | cut -d " " -f1)
failing_tests=$(cat logs.txt | grep "^FAIL:" | cut -d"/"  -f2 | cut -d " " -f1)

for test in $passing_tests; do
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=pass>"
done

for test in $failing_tests; do
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test} RESULT=fail>"
done
