import gitlab
import os
import sys
import json
import base64

gitlab_url = os.environ.get("GITLAB_SERVER_URL", "https://git.morello-project.org")
private_token = os.environ.get("GITLAB_PRIVATE_TOKEN") or sys.exit(
    "GITLAB_PRIVATE_TOKEN not set"
)
gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
trigger_label = os.environ.get("TRIGGER_LABEL", "trigger-pipeline")
gitlab_group = os.environ.get("GITLAB_GROUP", "morello")
trigger_project_id = os.environ.get("TRIGGER_PROJECT_ID", "2")
trigger_project = gl.projects.get(trigger_project_id)
trigger_branch = os.environ.get("TRIGGER_BRANCH", "morello/test-trigger")
manifest_branch = "morello/mainline"
morello_group = gl.groups.get(gitlab_group)
mrs = morello_group.mergerequests.list(labels=[trigger_label], all=True)
mr_details = []
editable_mr_list = []
mr_web_urls = ""
for mr in mrs:
    # find the manifest branch if available
    for label in mr.labels:
        if label.startswith("manifest_branch="):
            manifest_branch = label.split("=")[1]
            branches = trigger_project.branches.list()
            manifest_branches = []
            for branch in branches:
                manifest_branches.append(branch.name)
            if manifest_branch not in manifest_branches:
                project = gl.projects.get(mr.project_id)
                project_mr = project.mergerequests.get(mr.iid)
                note = project_mr.notes.create({"body": "note content"})
                note.body = f"Failed to trigger pipeline since {manifest_branch} doesn't exist on manifest project"
                note.save()

    for label in mr.labels:
        if label.startswith("group-"):
            group_label = label
            group_mrs = morello_group.mergerequests.list(labels=[group_label], all=True)
            for group_mr in group_mrs:
                mr_detail = {}
                project = gl.projects.get(group_mr.project_id)
                mr_detail["project_path"] = project.path_with_namespace
                mr_detail["project_id"] = group_mr.project_id
                mr_detail["iid"] = group_mr.iid
                mr_detail["sha"] = group_mr.sha
                mr_detail["branch"] = group_mr.source_branch
                mr_details.append(mr_detail)
                mr_web_urls += f"\n\n{group_mr.web_url}"
                editable_mr_list.append(project.mergerequests.get(group_mr.iid))

            enc_data = base64.b64encode(json.dumps(mr_details).encode("ascii"))
            for trigger in trigger_project.triggers.list():
                if trigger.owner.get("name") == "merge-bot":
                    pipeline = trigger_project.trigger_pipeline(
                        trigger_branch,
                        trigger.token,
                        variables={
                            "PROJECT_REFS": enc_data.decode("ascii"),
                            "MANIFEST_BRANCH": manifest_branch,
                        },
                    )
                    print(pipeline)
                    break
            for editable_mr in editable_mr_list:
                editable_mr.labels = [
                    "triggered" if x == trigger_label else x for x in editable_mr.labels
                ]
                # This is for other merge requests
                editable_mr.labels.append("triggered")
                note = editable_mr.notes.create({"body": "note content"})
                note.body = f"Following Merge Requests grouped:{mr_web_urls} \n\nPipeline triggered on {manifest_branch} Manifest Branch: {pipeline.web_url}"
                print(note.body)
                note.save()
                editable_mr.save()
            # We consume only one "trigger-build" label everytime this script is run
            sys.exit()
else:
    print("No triggers found")
    sys.exit()
