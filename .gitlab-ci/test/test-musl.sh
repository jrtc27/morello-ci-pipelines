#!/bin/sh

MUSL_PROJECT_BRANCH="morello/master"
if [ ${CI_PROJECT_PATH} == "morello/musl-libc" ]; then
    MUSL_PROJECT_BRANCH="${CI_COMMIT_REF_NAME:-$MUSL_PROJECT_BRANCH}"
fi
tar -xvf ${CI_PROJECT_DIR}/morello-clang-aarch64.tar.xz
tar -xvf ${CI_PROJECT_DIR}/musl-libc.tar.xz
export MUSL_ROOT=$PWD/musl
export MORELLOIE=$(which morelloie)
export CC=$PWD/clang-current/bin/clang
git clone https://git.morello-project.org/morello/musl-libc -b ${MUSL_PROJECT_BRANCH} ${CI_PROJECT_DIR}/musl-libc
cd ${CI_PROJECT_DIR}/musl-libc
make -C test test
