#!/bin/sh

export LAVA_SERVER=${LAVA_SERVER:-https://lava.morello-project.org/}
export NET_ID="net-${BUILD_JOB_ID}-${RANDOM}"
export TC_URL="${TC_URL}"

# lavacli identities add \
#   --username ${CI_LAVA_USER} \
#   --token ${CI_LAVA_TOKEN} \
#   --uri ${LAVA_SERVER}/RPC2 \
#   default

${CI_PROJECT_DIR}/lava/submit_for_testing.py \
  --template-path ${CI_PROJECT_DIR}/lava/templates \
  --template-name ${LAVA_TEMPLATE_NAME} \
  --lava-server ${LAVA_SERVER} \
  --lava-token ${CI_LAVA_TOKEN}

# lavacli jobs submit --follow ${CI_PROJECT_DIR}/lava/templates/${LAVA_TEMPLATE_NAME}
# lavacli jobs wait
# lavacli jobs show
