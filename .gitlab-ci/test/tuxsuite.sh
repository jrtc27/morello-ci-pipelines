#!/bin/sh

BASE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${BUILD_JOB_ID}/artifacts"
LLDB_URL="$BASE_URL/lldb_tests.tar.xz"
ROOTFS="$BASE_URL/android-nano.img.xz"
USERDATA="$BASE_URL/userdata.tar.xz"
PARAMETERS=""
TESTS=""

case $TEST_NAME in
    binder)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA"
        TESTS="--tests binder"
        ;;
    bionic)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA --parameters GTEST_FILTER=$GTEST_FILTER"
        TESTS="--tests bionic"
        ;;
    compartment)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA"
        TESTS="--tests compartment"
        ;;
    device-tree)
        DEVICE="fvp-morello-android"
        TESTS="--tests device-tree"
        ;;
    dvfs)
        DEVICE="fvp-morello-android"
        TESTS="--tests dvfs"
        ;;
    lldb)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters LLDB_URL=$LLDB_URL --parameters TC_URL=$TC_URL"
        TESTS="--tests lldb"
        ;;
    logd)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA"
        TESTS="--tests logd"
        ;;
    multicore)
        DEVICE="fvp-morello-android"
        TESTS="--tests multicore"
        ;;
    busybox)
        DEVICE="fvp-morello-busybox"
        ROOTFS="$BASE_URL/busybox.img.xz"
        ;;
    poky)
        DEVICE="fvp-morello-oe"
        ROOTFS="$BASE_URL/core-image-minimal-morello-fvp.wic"
        TESTS="--tests fwts"
        ;;
    *)
        echo "Unknown TEST_NAME '$TEST_NAME'"
        exit 1
        ;;
esac

tuxsuite test \
    --device "$DEVICE" \
    --mcp-fw "$BASE_URL/mcp_fw.bin" \
    --mcp-romfw "$BASE_URL/mcp_romfw.bin" \
    --rootfs "$ROOTFS" \
    --scp-fw "$BASE_URL/scp_fw.bin" \
    --scp-romfw "$BASE_URL/scp_romfw.bin" \
    --uefi "$BASE_URL/uefi.bin" $TESTS $PARAMETERS
