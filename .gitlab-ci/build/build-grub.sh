#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

# For convenience, add gnulib and grub to the bsp group
xmlstarlet edit --inplace \
  --update "//project[@name='gnulib']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml
xmlstarlet edit --inplace \
  --update "//project[@name='grub']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh

# Build GRUB
time bash -x ./build-scripts/fetch-tools.sh -f none
time bash -x ./build-scripts/build-grub.sh -f none

cp -a output/fvp/intermediates/grub.efi ${CI_PROJECT_DIR}

# Create SHA256SUMS.txt file
sha256sum grub.efi > SHA256SUMS.txt
