#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
TC_URL=${TC_URL:-}

install_custom_toolchain()
{
  test -z "${TC_URL}" && return 0
  TC="${CI_PROJECT_DIR}/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  rm -rf ${CI_PROJECT_DIR}/tools/clang ${CI_PROJECT_DIR}/tools/.clang.*
  TC_DIR="${CI_PROJECT_DIR}/tools/clang/bin"
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  printf "INFO: Custom toolchain installed from \n%s\n" "${TC_URL}"
  which clang
  clang --version
}

if [ "${PIPELINE}" = "toolchain" ]; then
  FW_URL="https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/${CI_PIPELINES_BRANCH}/raw"
  FW_DIR="output/fvp/firmware"
  FW_FILES="mcp_fw.bin mcp_romfw.bin scp_fw.bin scp_romfw.bin uefi.bin"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=build-firmware"
  done
  FW_DIR="output/fvp/intermediates"
  FW_FILES="grub.efi morello.dtb"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=build-firmware"
  done
fi

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g busybox \
  --repo-rev=v2.16

repo selfupdate
repo version

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
mkdir -p tools/arm_gcc/bin
touch tools/arm_gcc/bin/arm-none-eabi-gcc
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
# Install custom toolchain
install_custom_toolchain

# Build Busybox image
time bash -x ./build-scripts/fetch-tools.sh -f busybox
time bash -x ./build-scripts/build-linux.sh -f busybox
time bash -x ./build-scripts/build-busybox.sh -f busybox
time bash -x ./build-scripts/build-disk-image.sh -f busybox
# Alternatively, we can run
# time bash -x ./build-scripts/build-all.sh -f busybox

# Prepare files to publish
cp -a \
  output/fvp/busybox.img \
  output/fvp/firmware/*.bin \
  ${CI_PROJECT_DIR}
# Compress the image
xz busybox.img

# Create SHA256SUMS.txt file
sha256sum *.bin *.img.xz *.xml > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-busybox.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
