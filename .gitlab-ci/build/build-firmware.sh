#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
TC_URL=${TC_URL:-}

install_custom_toolchain()
{
  test -z "${TC_URL}" && return 0
  TC="${CI_PROJECT_DIR}/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  rm -rf ${CI_PROJECT_DIR}/tools/clang ${CI_PROJECT_DIR}/tools/.clang.*
  TC_DIR="${CI_PROJECT_DIR}/tools/clang/bin"
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  printf "INFO: Custom toolchain installed from \n%s\n" "${TC_URL}"
  which clang
  clang --version
}

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

case "$CI_PROJECT_PATH" in
  morello/build-scripts)
    BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-morello/mainline}
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${BUILD_SCRIPTS_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
esac

if [ ! -z ${PROJECT_REFS+x} ]; then
  ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
  for manifest in $(ls .repo/manifests/); do
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/${manifest}
  done
fi

# For convenience, add gnulib and grub to the bsp group
xmlstarlet edit --inplace \
  --update "//project[@name='gnulib']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml
xmlstarlet edit --inplace \
  --update "//project[@name='grub']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
# Install custom toolchain
install_custom_toolchain

# Build the firmwares
time bash -x ./build-scripts/fetch-tools.sh -f none
time bash -x ./build-scripts/build-scp.sh -f none
time bash -x ./build-scripts/build-arm-tf.sh -f none
time bash -x ./build-scripts/build-uefi.sh -f none
time bash -x ./build-scripts/build-firmware-image.sh -f none
# Alternatively, we can run
# time bash -x ./build-scripts/build-all.sh -f none

# Build GRUB
time bash -x ./build-scripts/build-grub.sh -f none

echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-ubuntu.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
