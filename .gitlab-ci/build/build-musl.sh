#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
CHECK_TESTS="${CHECK_TESTS:-"check-llvm check-clang check-lld check-lldb"}"
MUSL_PROJECT_BRANCH="morello/master"
LIBSHIM_PROJECT_BRANCH="morello/mainline"
LIBARCHCAP_PROJECT_BRANCH="morello/mainline"

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "INFO: Set http.cookiefile\n"
fi

set -ex

if [ -z "${SKIP_UPDATE}" ]; then
  rm -rf .repo/manifests
  repo init --no-clone-bundle \
    -u https://git.morello-project.org/morello/manifest.git \
    -b ${MANIFEST_BRANCH} -g toolchain-src \
    --repo-rev=v2.16

  repo selfupdate
  repo version

  # Set the llvm-project revision
  case "$CI_PROJECT_PATH" in
    morello/llvm-project)
      LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/master}"
      TAG=$(curl -s https://git.morello-project.org/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${LLVM_PROJECT_BRANCH}|jq '.name // empty')
      if [ ! -z $TAG ]; then
          LLVM_PROJECT_BRANCH=refs/tags/${LLVM_PROJECT_BRANCH}
      fi
      xmlstarlet edit --inplace \
        --update "//project[@name='llvm-project']/@revision" \
        --value ${LLVM_PROJECT_BRANCH} \
        .repo/manifests/morello-toolchain.xml
    ;;
    morello/musl-libc)
      MUSL_PROJECT_BRANCH="${CI_COMMIT_REF_NAME:-$MUSL_PROJECT_BRANCH}"
      ;;
    morello/android/platform/external/libshim)
      export LIBSHIM_REF="${CI_COMMIT_REF_NAME:-$LIBSHIM_PROJECT_BRANCH}"
      ;;
    morello/android/platform/external/libarchcap)
      export LIBARCHCAP_REF="${CI_COMMIT_REF_NAME:-$LIBARCHCAP_PROJECT_BRANCH}"
      ;;
  esac

  if [ ! -z ${PROJECT_REFS+x} ]; then
    ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/morello-toolchain.xml
  fi

  # Avoid to download +12G of prebuilt binaries
  sed -i '/darwin/d' .repo/manifests/morello-toolchain.xml
  sed -i '/mingw32/d' .repo/manifests/morello-toolchain.xml
  sed -i '/windows/d' .repo/manifests/morello-toolchain.xml
  if [ -z "${ANDROID_GIT_COOKIE}" ]; then
    printf "INFO: Skip http.cookiefile\n"
  else
    xmlstarlet edit --inplace  \
      --update "//remote[@name='aosp']/@fetch" \
      --value "https://android.googlesource.com/a/" \
      .repo/manifests/remotes.xml
  fi

  time repo sync -j8 --quiet --no-clone-bundle
  repo manifest -r -o pinned-manifest.xml
  cat pinned-manifest.xml
fi

which cmake
cmake --version
which python
python --version
which python3
python3 --version

# where host LLVM is installed
export HOST_LLVM_BIN=/usr/lib/llvm-10/bin
# path to sources of the Morello toolchain
export LLVM_PROJECT="$(pwd)/toolchain-src/toolchain/llvm-project"
# target installation path for the Morello toolchain
export MORELLO_HOME="${CI_PROJECT_DIR}/clang-current"

mkdir build && cd build
cmake \
   -DCMAKE_C_COMPILER=${HOST_LLVM_BIN}/clang \
   -DCMAKE_C_COMPILER_WORKS=YES \
   -DCMAKE_CXX_COMPILER=${HOST_LLVM_BIN}/clang++ \
   -DCMAKE_CXX_COMPILER_WORKS=YES \
   -DCMAKE_AR=${HOST_LLVM_BIN}/llvm-ar \
   -DCMAKE_RANLIB=${HOST_LLVM_BIN}/llvm-ranlib \
   -DCMAKE_NM=${HOST_LLVM_BIN}/llvm-nm \
   -DCMAKE_LINKER=${HOST_LLVM_BIN}/ld.lld \
   -DCMAKE_OBJDUMP=${HOST_LLVM_BIN}/llvm-objdump \
   -DCMAKE_OBJCOPY=${HOST_LLVM_BIN}/llvm-objcopy \
   -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
   -DCMAKE_SHARED_LINKER_FLAGS="-fuse-ld=lld" \
   -DLLVM_TARGETS_TO_BUILD="AArch64" \
   -DCMAKE_BUILD_TYPE=Release \
   -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
   -DBUILD_SHARED_LIBS=ON \
   -DCMAKE_SKIP_BUILD_RPATH=OFF \
   -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib \
   -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
   -DLLVM_ENABLE_ASSERTIONS=ON \
   -DLLVM_ENABLE_LIBCXX=ON \
   -DLLVM_ENABLE_LLD=ON \
   -DLIBCXX_CXX_ABI=libcxxabi \
   -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${LLVM_PROJECT}/libcxxabi/include" \
   -DLIBCXXABI_USE_LLVM_UNWINDER=ON \
   -DLIBCXX_USE_COMPILER_RT=ON \
   -DLIBCXXABI_USE_COMPILER_RT=ON \
   -DLIBCXX_ENABLE_THREADS=ON \
   -DLIBCXXABI_ENABLE_THREADS=ON \
   -DLIBUNWIND_ENABLE_THREADS=ON \
   -DCMAKE_INSTALL_PREFIX=${MORELLO_HOME} \
   -DLLVM_ENABLE_EH=ON -DLLVM_ENABLE_RTTI=ON \
   -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;libcxx;libcxxabi;compiler-rt;libunwind" \
   ${LLVM_PROJECT}/llvm

make -j16
make install

# Workaround musl not able to pick llvm-ar
export PATH=$PATH:$CI_PROJECT_DIR/clang-current/bin/
# Build musl
cd $CI_PROJECT_DIR
git clone https://git.morello-project.org/morello/musl-libc -b $MUSL_PROJECT_BRANCH
cd musl-libc
export MUSL_HOME=$CI_PROJECT_DIR/musl
CC=${MORELLO_HOME}/bin/clang ./configure \
    --disable-shared --enable-morello --enable-libshim  --prefix=${MUSL_HOME}
make
make install

# Compiling crtbegin and crtend objects
${MORELLO_HOME}/bin/clang -march=morello+c64 -mabi=purecap \
    -c ${LLVM_PROJECT}/compiler-rt/lib/crt/crtbegin.c \
    -o ${MORELLO_HOME}/lib/clang/11.0.0/lib/linux/clang_rt.crtbegin-morello.o

${MORELLO_HOME}/bin/clang -march=morello+c64 -mabi=purecap \
    -c ${LLVM_PROJECT}/compiler-rt/lib/crt/crtend.c \
    -o ${MORELLO_HOME}/lib/clang/11.0.0/lib/linux/clang_rt.crtend-morello.o

# Compiling libclang_rt.builtins-morello.a

mkdir build-rt && cd build-rt

echo 'set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_C_COMPILER_TARGET "aarch64-linux-gnueabi -march=morello+c64 -mabi=purecap")' > toolchain.cmake

cmake -Wno-dev \
   -DCMAKE_TOOLCHAIN_FILE=$PWD/toolchain.cmake \
   -DCOMPILER_RT_DEFAULT_TARGET_TRIPLE=aarch64-linux-gnueabi \
   -DCMAKE_C_FLAGS="-nostdinc -isystem ${MUSL_HOME}/include" \
   -DLLVM_CONFIG_PATH=${MORELLO_HOME}/bin/llvm-config \
   -DCMAKE_C_COMPILER=${MORELLO_HOME}/bin/clang \
   -DCMAKE_C_COMPILER_WORKS=YES \
   -DCMAKE_CXX_COMPILER=${MORELLO_HOME}/bin/clang++ \
   -DCMAKE_CXX_COMPILER_WORKS=YES \
   -DCMAKE_AR=${MORELLO_HOME}/bin/llvm-ar \
   -DCMAKE_RANLIB=${MORELLO_HOME}/bin/llvm-ranlib \
   -DCMAKE_NM=${MORELLO_HOME}/bin/llvm-nm \
   -DCMAKE_LINKER=${MORELLO_HOME}/bin/ld.lld \
   -DCMAKE_OBJDUMP=${MORELLO_HOME}/bin/llvm-objdump \
   -DCMAKE_OBJCOPY=${MORELLO_HOME}/bin/llvm-objcopy \
   -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
   -DCMAKE_SHARED_LINKER_FLAGS="-fuse-ld=lld" \
   -DLLVM_TARGETS_TO_BUILD="AArch64" \
   -DCMAKE_BUILD_TYPE=Release \
   -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
   -DBUILD_SHARED_LIBS=ON \
   -DCMAKE_SKIP_BUILD_RPATH=OFF \
   -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib \
   -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
   -DLLVM_ENABLE_ASSERTIONS=ON \
   -DCMAKE_INSTALL_PREFIX=${MORELLO_HOME}-rt \
   -DCOMPILER_RT_BUILD_SANITIZERS=OFF \
   -DCOMPILER_RT_BUILD_XRAY=OFF \
   -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
   ${LLVM_PROJECT}/compiler-rt

make clang_rt.builtins-aarch64

mv lib/linux/libclang_rt.builtins-aarch64.a ${MORELLO_HOME}/lib/clang/11.0.0/lib/linux/libclang_rt.builtins-morello.a

if [ -z "${SKIP_ARCHIVE}" ]; then
  # Compress the toolchain
  cd ${CI_PROJECT_DIR}
  time tar -cJf ${CI_PROJECT_DIR}/morello-clang-aarch64.tar.xz clang-current/
  time tar -cJf ${CI_PROJECT_DIR}/musl-libc.tar.xz musl/
  # Create SHA256SUMS.txt file
  sha256sum *.tar.xz *.xml > SHA256SUMS.txt
fi
