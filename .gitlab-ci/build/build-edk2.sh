#!/bin/sh

#git submodule sync --recursive
#git submodule update --init --recursive

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

# Set EDK2 revision
case "$CI_PROJECT_PATH" in
  morello/edk2)
    EDK2_BRANCH="${EDK2_BRANCH:-morello/master}"
    xmlstarlet edit --inplace \
      --update "//project[@name='edk2']/@revision" \
      --value ${EDK2_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
  morello/edk2-platforms)
    EDK2_PLATFORM_BRANCH="${EDK2_PLATFORM_BRANCH:-morello/master}"
    xmlstarlet edit --inplace \
      --update "//project[@name='edk2-platforms']/@revision" \
      --value ${EDK2_PLATFORM_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
esac

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh

# Build EDK2
time bash -x ./build-scripts/fetch-tools.sh -f none
time bash -x ./build-scripts/build-uefi.sh -f none

# Build rest of firmware for ubuntu distro testing
time bash -x ./build-scripts/build-scp.sh -f none
time bash -x ./build-scripts/build-arm-tf.sh -f none
time bash -x ./build-scripts/build-firmware-image.sh -f none

cp -a output/fvp/intermediates/uefi.bin ${CI_PROJECT_DIR}

# Create SHA256SUMS.txt file
sha256sum uefi.bin > SHA256SUMS.txt
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-ubuntu.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
