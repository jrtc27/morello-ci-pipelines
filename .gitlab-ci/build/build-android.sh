#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
TC_URL=${TC_URL:-}
# soong_ui.bash gets messed up identifying top level dir for soong. Removing all files except artifacts
# from previous stage
shopt -s extglob
if [ "${CI_PROJECT_NAME}" = "soong" ]; then
	rm -rf !(output)
fi
install_custom_toolchain()
{
  test -z "${TC_URL}" && return 0
  TC="${CI_PROJECT_DIR}/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  rm -rf ${CI_PROJECT_DIR}/tools/clang ${CI_PROJECT_DIR}/tools/.clang.*
  TC_DIR="${CI_PROJECT_DIR}/tools/clang/bin"
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  printf "INFO: Custom toolchain installed from \n%s\n" "${TC_URL}"
  which clang
  clang --version
}

if [ "${PIPELINE}" = "toolchain" ]; then
  FW_URL="https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/${CI_PIPELINES_BRANCH}/raw"
  FW_DIR="output/fvp/firmware"
  FW_FILES="mcp_fw.bin mcp_romfw.bin scp_fw.bin scp_romfw.bin uefi.bin"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=build-firmware"
  done
  FW_DIR="output/fvp/intermediates"
  FW_FILES="grub.efi morello.dtb"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=build-firmware"
  done
fi

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "INFO: Set http.cookiefile\n"
fi

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g android \
  --repo-rev=v2.16

repo selfupdate
repo version

# Set the Android project revision
case "$CI_PROJECT_PATH" in
  morello/kernel/morello-ack)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/build-scripts)
    BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-morello/mainline}
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${BUILD_SCRIPTS_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
  morello/android/device/arm/morello)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/vendor/arm/morello-examples)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/vendor/arm/tools)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libshim)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libarchcap)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/art)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/bionic)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/build/make)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/build/soong)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/boringssl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/bzip2)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/compiler-rt)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/conscrypt)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/dnsmasq)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/e2fsprogs)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/freetype)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/googletest)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/icu)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/jemalloc_new)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/kernel-headers)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libcxx)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libcxxabi)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libjpeg-turbo)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libpng)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libunwind_llvm)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/llvm)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/lzma)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/pdfium)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/selinux)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/sqlite)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/tcpdump)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/toybox)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/zlib)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/frameworks/base)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/frameworks/native)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/hardware/libhardware)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/core)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/libhidl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/testing/gtest_extras)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/tools/hidl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
esac

if [ ! -z ${PROJECT_REFS+x} ]; then
  ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
  for manifest in $(ls .repo/manifests/); do
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/${manifest}
  done
fi

# Avoid to download +XXG of prebuilt binaries
sed -i '/darwin/d' .repo/manifests/android.xml
sed -i '/windows/d' .repo/manifests/android.xml
if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  xmlstarlet edit --inplace  \
    --update "//remote[@name='aosp']/@fetch" \
    --value "https://android.googlesource.com/a/" \
    .repo/manifests/remotes.xml
fi

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
mkdir -p tools/arm_gcc/bin
touch tools/arm_gcc/bin/arm-none-eabi-gcc
mkdir -p tools/linaro_gcc/bin
touch tools/linaro_gcc/bin/aarch64-linux-gnu-gcc
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
# Install custom toolchain
install_custom_toolchain

# Include extra packages to the image
# cat << EOF >> android/device/arm/morello/morello_fvp_nano.mk
#
# PRODUCT_PACKAGES += \\
#     bionic-unit-tests \\
#     bionic-unit-tests-static \\
#     binderDriverInterfaceTest \\
#     binderLibTest \\
#     binderSafeInterfaceTest \\
#     binderTextOutputTest \\
#     binderThroughputTest \\
#     liblog-unit-tests \\
#     liblog-benchmarks \\
#     logd-unit-tests \\
#     compartment-demo \\
#
# EOF

# Build Android
time bash -x ./build-scripts/fetch-tools.sh -f android-nano
time bash -x ./build-scripts/build-linux.sh -f android-nano
time bash -x ./build-scripts/build-android.sh -f android-nano

# Alternatively, we can run
# time bash -x ./build-scripts/build-all.sh -f android-nano

# Build tests
cd android
if [ "${CI_PROJECT_NAME}" = "llvm-project" ]; then
        LLVM_BRANCH="${CI_COMMIT_REF_NAME}"
else
        LLVM_BRANCH="morello/master"
fi
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/lldb_tests.tar.bz2 \
    https://git.morello-project.org/morello/llvm-project/-/archive/${LLVM_BRANCH}/llvm-project-morello-dev.tar.bz2?path=lldb/morello-android-tests
mkdir test/lldb && tar -xf /tmp/lldb_tests.tar.bz2 -C test/lldb --strip-components=3
export ENVSETUP_NO_COMPLETION=adb:fastboot:asuite
source build/envsetup.sh
time lunch morello_fvp_nano-eng
mmma test/lldb
TESTS="bionic-unit-tests bionic-unit-tests-static"
TESTS="${TESTS} binderDriverInterfaceTest binderLibTest binderSafeInterfaceTest"
TESTS="${TESTS} binderTextOutputTest binderThroughputTest"
TESTS="${TESTS} liblog-unit-tests liblog-benchmarks logd-unit-tests"
for test in ${TESTS}; do
  m -j ${test}
done
for example in $(ls -dR vendor/arm/morello-examples/*/); do
  example=$(echo ${example} | cut -d'/' -f4)
  m -j ${example}
done
m -j userdataimage-nodeps

mkdir lldb_tests/
cp -rf out/target/product/morello/data/ \
       out/target/product/morello/symbols/ \
       lldb_tests/
cp -rf test/lldb/ lldb_tests/
tar -cJf lldb_tests.tar.xz lldb_tests/
cp lldb_tests.tar.xz ${CI_PROJECT_DIR}

cd ${CI_PROJECT_DIR}

# Generate Android images
time bash -x ./build-scripts/build-disk-image.sh -f android-nano

# Prepare files to publish
cp -a \
  android/out/target/product/morello/userdata.img \
  output/fvp/android-nano.img \
  output/fvp/firmware/*.bin \
  ${CI_PROJECT_DIR}
(cd android/out/target/product/morello; time tar -cJf ${CI_PROJECT_DIR}/userdata.tar.xz data)

# Compress the image
xz android-nano.img
xz userdata.img
# Create SHA256SUMS.txt file
sha256sum *.bin *.img.xz *.xml lldb_tests.tar.xz userdata.tar.xz > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "TC_URL=${TC_URL:-https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/raw/lldb_tests.tar.xz?job=build-android}" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
